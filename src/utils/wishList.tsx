import { WishListItem } from '../types';

// eslint-disable-next-line import/prefer-default-export
export const productInWishList = (id: number, wishList: WishListItem[]): boolean => (
  wishList.some((item) => item.productId === id)
);
