// eslint-disable-next-line import/prefer-default-export
export const formatPrice = (price: number) => {
  const formattedPrice = price;
  return `€ ${formattedPrice}`;
};
