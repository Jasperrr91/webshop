import type { Product } from '../types';

// eslint-disable-next-line import/prefer-default-export
export const getProduct = (products: Product[], id: number) => (
  products.filter((product) => {
    if (product.id === id) return true;
    return false;
  })[0]
);
