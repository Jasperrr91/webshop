import React from 'react';
import { Provider } from 'react-redux';

import store from './store/store';
import './App.less';
import ProductList from './components/ProductList';
import Logo from './svg/Logo';
import FavoriteToggle from './components/FavoriteToggle';
import WishList from './components/WishList';
import CartToggle from './components/CartToggle';
import CartList from './components/CartList';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <header className="header">
          <div className="header-bar">
            <div className="logo">
              <Logo />
            </div>
            <FavoriteToggle />
            <CartToggle />
          </div>
        </header>
        <section className="content">
          <ProductList />
          <WishList />
          <CartList />
        </section>
      </div>
    </Provider>
  );
}

export default App;
