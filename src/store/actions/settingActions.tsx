import { SettingActionTypes } from './actionTypes';

const {
  TOGGLE_CART,
  TOGGLE_WISHLIST,
} = SettingActionTypes;

export function toggleCart() {
  return { type: TOGGLE_CART, payload: null };
}

export function toggleWishList() {
  return { type: TOGGLE_WISHLIST, payload: null };
}
