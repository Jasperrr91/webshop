import { WishListActionTypes } from './actionTypes';

const {
  ADD_TO_WISHLIST,
  REMOVE_FROM_WISHLIST,
} = WishListActionTypes;

export function addToWishList(id: number) {
  return { type: ADD_TO_WISHLIST, payload: id };
}

export function removeFromWishList(id: number) {
  return { type: REMOVE_FROM_WISHLIST, payload: id };
}
