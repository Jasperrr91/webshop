import { ProductActionTypes } from './actionTypes';

import type { Product } from '../../types';

const { ADD_PRODUCT } = ProductActionTypes;

// eslint-disable-next-line import/prefer-default-export
export function addProduct(product: Product) {
  return { type: ADD_PRODUCT, payload: product };
}
