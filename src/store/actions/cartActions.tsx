import { CartActionTypes } from './actionTypes';

import type { CartOrder } from '../../types';

const {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  SET_QUANTITY,
} = CartActionTypes;

export function addToCart(order: CartOrder) {
  return { type: ADD_TO_CART, payload: order };
}

export function removeFromCart(id: number) {
  return { type: REMOVE_FROM_CART, payload: id };
}

export function setQuantity(order: CartOrder) {
  return { type: SET_QUANTITY, payload: order };
}
