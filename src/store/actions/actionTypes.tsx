export const CartActionTypes = {
  ADD_TO_CART: 'ADD_TO_CART',
  REMOVE_FROM_CART: 'REMOVE_FROM_CART',
  SET_QUANTITY: 'SET_QUANTITY',
};

export const ProductActionTypes = {
  ADD_PRODUCT: 'ADD_PRODUCT',
};

export const SettingActionTypes = {
  TOGGLE_CART: 'TOGGLE_CART',
  TOGGLE_WISHLIST: 'TOGGLE_WISHLIST',
};

export const WishListActionTypes = {
  ADD_TO_WISHLIST: 'ADD_TO_WISHLIST',
  REMOVE_FROM_WISHLIST: 'REMOVE_FROM_WISHLIST',
};
