import { CartActionTypes } from '../actions/actionTypes';

import { productInWishList } from '../../utils';

import type { Cart } from '../../types';

const {
  ADD_TO_CART,
  REMOVE_FROM_CART,
  SET_QUANTITY,
} = CartActionTypes;

type Keys = keyof typeof CartActionTypes;

type CartAction = {
  type: Keys,
  payload: any,
};

const initialState = {
  items: [],
  totalCount: 0,
  totalPrice: 0,
};

const cartReducer = (state: Cart = initialState, action: CartAction) => {
  switch (action.type) {
    case ADD_TO_CART:
      if (!productInWishList(action.payload.productId, state.items)) {
        // Add item to wishlist with default quantity of 1
        return {
          ...state,
          totalCount: state.totalCount + action.payload.quantity,
          items: [
            ...state.items,
            {
              productId: action.payload.productId,
              quantity: action.payload.quantity,
            },
          ],
        };
      }
      return {
        ...state,
        totalCount: state.totalCount + action.payload.quantity,
        items: [
          ...state.items.map((item) => {
            if (item.productId === action.payload.productId) {
              return {
                ...item,
                quantity: item.quantity + action.payload.quantity,
              };
            }
            return item;
          }),
        ],
      };
    case REMOVE_FROM_CART:
      return {
        ...state,
        items: [
          ...state.items.filter((item) => {
            if (item.productId === action.payload) return false;
            return true;
          }),
        ],
        totalCount: state.totalCount - state.items.filter((item) => {
          if (item.productId === action.payload) return true;
          return false;
        })[0].quantity,
      };
    case SET_QUANTITY:
      return {
        ...state,
        items: [
          ...state.items.map((item) => {
            if (item.productId === action.payload.productId) {
              return {
                ...item,
                quantity: action.payload.quantity,
              };
            }
            return item;
          }),
        ],
        totalCount: state.totalCount + (action.payload.quantity - state.items.filter((item) => {
          if (item.productId === action.payload.productId) return true;
          return false;
        })[0].quantity),
      };
    default:
      return state;
  }
};

export type CartState = ReturnType<typeof cartReducer>;

export default cartReducer;
