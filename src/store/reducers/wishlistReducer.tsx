import { WishListActionTypes } from '../actions/actionTypes';
import { productInWishList } from '../../utils';

import type { WishList } from '../../types';

const {
  ADD_TO_WISHLIST,
  REMOVE_FROM_WISHLIST,
} = WishListActionTypes;

type Keys = keyof typeof WishListActionTypes;

type WishListAction = {
  type: Keys,
  payload: any,
};

const initialState = {
  items: [],
  visible: false,
  count: 0,
};

const wishListReducer = (state: WishList = initialState, action: WishListAction) => {
  switch (action.type) {
    case ADD_TO_WISHLIST:
      if (!productInWishList(action.payload, state.items)) {
        // Add item to wishlist with default quanitity of 1
        return {
          ...state,
          count: state.count + 1,
          items: [
            ...state.items,
            {
              productId: action.payload,
              quantity: 1,
            },
          ],
        };
      }
      return { ...state };
    case REMOVE_FROM_WISHLIST:
      return {
        ...state,
        count: state.count - 1,
        items: [
          ...state.items.filter((item) => {
            if (item.productId === action.payload) return false;
            return true;
          }),
        ],
      };
    default:
      return state;
  }
};

export type WishListState = ReturnType<typeof wishListReducer>;

export default wishListReducer;
