import { SettingActionTypes } from '../actions/actionTypes';

import type { Setting } from '../../types';

const {
  TOGGLE_CART,
  TOGGLE_WISHLIST,
} = SettingActionTypes;

type Keys = keyof typeof SettingActionTypes;

type SettingAction = {
  type: Keys,
  payload: any,
};

const initialState = {
  showCart: false,
  showWishList: false,
};

const settingReducer = (state: Setting = initialState, action: SettingAction) => {
  switch (action.type) {
    case TOGGLE_CART:
      return {
        ...state,
        showCart: !state.showCart,
        showWishList: false,
      };
    case TOGGLE_WISHLIST:
      return {
        ...state,
        showCart: false,
        showWishList: !state.showWishList,
      };
    default:
      return state;
  }
};

export type SettingState = ReturnType<typeof settingReducer>;

export default settingReducer;
