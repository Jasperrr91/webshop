import faker from 'faker';
import { ProductActionTypes } from '../actions/actionTypes';

import type { Product } from '../../types';

type Keys = keyof typeof ProductActionTypes;

type ProductAction = {
  type: Keys,
  payload: any,
};

const nextId = (products: Product[]) => {
  if (products.length === 0) return 1;
  const currentMaxId = products.reduce(
    (maxId: number, product: any) => Math.max(product.id, maxId), -1,
  );
  return currentMaxId + 1;
};

const initialState = (): any => {
  const products: any = {};

  for (let i = 1; i <= 20; i += 1) {
    products[i] = {
      id: i,
      name: faker.commerce.productName(),
      price: parseInt(faker.commerce.price(), 10),
      image: `https://source.unsplash.com/collection/4530416/240x240?sig=${i}`,
    };
  }

  return products;
};

const { ADD_PRODUCT } = ProductActionTypes;

const productReducer = (state: Product[] = initialState(), action: ProductAction) => {
  switch (action.type) {
    case ADD_PRODUCT:
      return [
        ...state,
        {
          ...action.payload,
          id: nextId(state),
        },
      ];
    default:
      return state;
  }
};

export type ProductState = ReturnType<typeof productReducer>;

export default productReducer;
