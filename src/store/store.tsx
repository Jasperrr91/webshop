/* eslint-disable no-undef */
/* eslint-disable import/no-unresolved */
import { combineReducers, createStore, compose } from 'redux';
import cartReducer from './reducers/cartReducer';
import productReducer from './reducers/productReducer';
import settingReducer from './reducers/settingReducer';
import wishListReducer from './reducers/wishListReducer';

import type { CartState } from './reducers/cartReducer';
import type { ProductState } from './reducers/productReducer';
import type { SettingState } from './reducers/settingReducer';
import type { WishListState } from './reducers/wishListReducer';

export type RootState = {
  cart: CartState,
  products: ProductState,
  settings: SettingState,
  wishList: WishListState,
};

function saveToLocalStorage(state: RootState) {
  try {
    const serialisedState = JSON.stringify(state);
    localStorage.setItem('persistantState', serialisedState);
  } catch (e) {
    console.warn(e);
  }
}

function loadFromLocalStorage() {
  try {
    const serialisedState = localStorage.getItem('persistantState');
    if (serialisedState === null) return undefined;
    return JSON.parse(serialisedState);
  } catch (e) {
    console.warn(e);
    return undefined;
  }
}

const rootReducer = combineReducers({
  cart: cartReducer,
  products: productReducer,
  settings: settingReducer,
  wishList: wishListReducer,
});

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

// eslint-disable-next-line no-underscore-dangle
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  rootReducer,
  loadFromLocalStorage(),
  composeEnhancers(),
);

store.subscribe(() => saveToLocalStorage(store.getState()));

export default store;
