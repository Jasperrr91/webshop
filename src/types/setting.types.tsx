export type Setting = {
  showCart: boolean,
  showWishList: boolean,
};
