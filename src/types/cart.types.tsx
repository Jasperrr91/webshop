export type CartOrder = {
  productId: number,
  quantity: number,
};

export type Cart = {
  items: CartItem[],
  totalCount: number,
  totalPrice: number,
};

export type CartItem = {
  productId: number,
  quantity: number,
};
