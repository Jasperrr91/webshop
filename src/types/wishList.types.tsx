export type WishList = {
  items: WishListItem[],
  visible: boolean,
  count: number,
};

export type WishListItem = {
  productId: number,
  quantity: number,
};
