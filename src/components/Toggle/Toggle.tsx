import React, { ForwardRefExoticComponent } from 'react';
import { Badge } from 'antd';
import { AntdIconProps } from '@ant-design/icons/lib/components/AntdIcon';

import './Toggle.less';

type Props = {
  IconFilled: ForwardRefExoticComponent<Pick<AntdIconProps, 'className' | 'onClick'>>,
  IconOutlined: ForwardRefExoticComponent<Pick<AntdIconProps, 'className' | 'onClick'>>,
  isVisible: boolean,
  count: number,
  dispatchToggle: () => void,
};

const renderToggle = (
  IconFilled: ForwardRefExoticComponent<Pick<AntdIconProps, 'className' | 'onClick'>>,
  IconOutlined: ForwardRefExoticComponent<Pick<AntdIconProps, 'className' | 'onClick'>>,
  isVisible: boolean,
  count: number,
  onClick: () => void,
) => {
  let className = 'toggle';
  if (count === 0) className += ' grayed-out';

  if (isVisible) {
    return <IconFilled className={className} onClick={onClick} />;
  }
  return <IconOutlined className={className} onClick={onClick} />;
};

const Toggle = ({
  IconFilled,
  IconOutlined,
  isVisible,
  count,
  dispatchToggle,
}: Props) => {
  const onClick = () => dispatchToggle();

  return (
    <Badge count={count}>
      {renderToggle(IconFilled, IconOutlined, isVisible, count, onClick)}
    </Badge>
  );
};

export default Toggle;
