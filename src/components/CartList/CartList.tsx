import React from 'react';
import { connect } from 'react-redux';
import { List, Avatar, Button } from 'antd';
import { DeleteOutlined, MinusOutlined, PlusOutlined } from '@ant-design/icons';

import { removeFromCart, setQuantity } from '../../store/actions/cartActions';
import { formatPrice } from '../../utils';

import './CartList.less';

import type { Product, CartItem, CartOrder } from '../../types';
import type { RootState } from '../../store/store';

type Props = {
  products: Product[],
  showCart: boolean,
  cartItems: CartItem[],
  dispatchCartUpdate: (arg: CartOrder) => void,
  dispatchCartRemove: (arg: number) => void,
};

const getCartActions = (
  item: CartItem,
  updateItem: (arg: CartOrder) => void,
  deleteItem: (arg: number) => void,
) => {
  const { productId, quantity } = item;
  const actions = [];

  const minusOne = () => updateItem({
    productId,
    quantity: quantity - 1,
  });

  const plusOne = () => updateItem({
    productId,
    quantity: quantity + 1,
  });

  if (quantity === 1) {
    actions.push(
      <Button type="primary" shape="circle" onClick={() => deleteItem(productId)}>
        <DeleteOutlined />
      </Button>,
    );
  } else {
    actions.push(
      <Button type="primary" shape="circle" onClick={() => minusOne()}>
        <MinusOutlined />
      </Button>,
    );
  }

  actions.push(
    <Button type="primary" shape="circle" onClick={() => plusOne()}>
      <PlusOutlined />
    </Button>,
  );

  return actions;
};

const RenderItem = (
  item: CartItem,
  products: Product[],
  dispatchCartUpdate: (arg: CartOrder) => void,
  dispatchCartRemove: (arg: number) => void,
) => {
  const product = products[item.productId];

  return (
    <List.Item
      actions={getCartActions(item, dispatchCartUpdate, dispatchCartRemove)}
    >
      <List.Item.Meta
        avatar={
          <Avatar src={product && product.image} />
        }
        title={product && product.name}
        description={formatPrice(product && product.price)}
      />
      <div>{item.quantity}</div>
    </List.Item>
  );
};

const CartList = ({
  products,
  showCart,
  cartItems,
  dispatchCartUpdate,
  dispatchCartRemove,
}: Props) => {
  if (showCart) {
    return (
      <div className="cart-list">
        <List
          itemLayout="horizontal"
          dataSource={cartItems}
          renderItem={(item) => RenderItem(
            item, products, dispatchCartUpdate, dispatchCartRemove,
          )}
        />
      </div>
    );
  }

  return null;
};

const mapStateToProps = (state: RootState) => ({
  products: state.products,
  showCart: state.settings.showCart,
  cartItems: state.cart.items,
});

const mapDispatchToProps = (dispatch: any) => ({
  dispatchCartUpdate: (order: CartOrder) => dispatch(setQuantity(order)),
  dispatchCartRemove: (id: number) => dispatch(removeFromCart(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CartList);
