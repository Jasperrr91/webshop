import React from 'react';
import { connect } from 'react-redux';
import { ShoppingOutlined, ShoppingFilled } from '@ant-design/icons';

import { toggleCart } from '../../store/actions/settingActions';
import Toggle from '../Toggle';

import type { RootState } from '../../store/store';

type Props = {
  showCart: boolean,
  cartCount: number,
  dispatchCartToggle: () => void,
};

const CartToggle = ({ showCart, cartCount, dispatchCartToggle }: Props) => (
  <Toggle
    IconFilled={ShoppingFilled}
    IconOutlined={ShoppingOutlined}
    count={cartCount}
    isVisible={showCart}
    dispatchToggle={dispatchCartToggle}
  />
);

const mapStateToProps = (state: RootState) => ({
  showCart: state.settings.showCart,
  cartCount: state.cart.totalCount,
});

const mapDispatchToProps = (dispatch: any) => ({
  dispatchCartToggle: () => dispatch(toggleCart()),
});

export default connect(mapStateToProps, mapDispatchToProps)(CartToggle);
