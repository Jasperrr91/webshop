import React from 'react';
import { connect } from 'react-redux';
import { List, Avatar, Button } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';

import { removeFromWishList } from '../../store/actions/wishListActions';
import { formatPrice } from '../../utils';

import './WishList.less';

import type { Product, WishListItem } from '../../types';
import type { RootState } from '../../store/store';

type Props = {
  products: Product[],
  showWishList: boolean,
  wishListItems: WishListItem[],
  dispatchWishListRemove: (arg: number) => void,
};

const RenderItem = (
  item: WishListItem,
  products: Product[],
  dispatchWishListRemove: (arg: number) => void,
) => {
  const product = products[item.productId];

  return (
    <List.Item
      actions={[
        <Button type="primary" shape="circle" onClick={() => dispatchWishListRemove(product.id)}>
          <DeleteOutlined />
        </Button>,
      ]}
    >
      <List.Item.Meta
        avatar={
          <Avatar src={product && product.image} />
        }
        title={product && product.name}
        description={formatPrice(product && product.price)}
      />
    </List.Item>
  );
};

const WishList = ({
  products,
  showWishList,
  wishListItems,
  dispatchWishListRemove,
}: Props) => {
  if (showWishList) {
    return (
      <div className="wish-list">
        <List
          itemLayout="horizontal"
          dataSource={wishListItems}
          renderItem={(item) => RenderItem(
            item, products, dispatchWishListRemove,
          )}
        />
      </div>
    );
  }

  return null;
};

const mapStateToProps = (state: RootState) => ({
  products: state.products,
  showWishList: state.settings.showWishList,
  wishListItems: state.wishList.items,
});

const mapDispatchToProps = (dispatch: any) => ({
  dispatchWishListRemove: (id: number) => dispatch(removeFromWishList(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(WishList);
