import React from 'react';
import { connect } from 'react-redux';
import { StarOutlined, StarFilled } from '@ant-design/icons';

import { toggleWishList } from '../../store/actions/settingActions';
import Toggle from '../Toggle';

import type { RootState } from '../../store/store';

type Props = {
  showWishList: boolean,
  favoriteCount: number,
  dispatchWishListToggle: () => void,
};

const FavoriteToggle = ({ showWishList, favoriteCount, dispatchWishListToggle }: Props) => (
  <Toggle
    IconFilled={StarFilled}
    IconOutlined={StarOutlined}
    count={favoriteCount}
    isVisible={showWishList}
    dispatchToggle={dispatchWishListToggle}
  />
);

const mapStateToProps = (state: RootState) => ({
  showWishList: state.settings.showWishList,
  favoriteCount: state.wishList.count,
});

const mapDispatchToProps = (dispatch: any) => ({
  dispatchWishListToggle: () => dispatch(toggleWishList()),
});

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteToggle);
