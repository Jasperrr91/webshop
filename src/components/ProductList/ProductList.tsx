import React from 'react';
import { connect } from 'react-redux';
import { Row, Col } from 'antd';

import ProductCard from '../ProductCard';
import './ProductList.css';

import type { Product } from '../../types';
import type { RootState } from '../../store/store';

type Props = {
  products: Product[],
};

const renderProductList = (products: Product[]): JSX.Element[] => {
  const productList: JSX.Element[] = [];

  // Chunk products into groups of 4 so we wrap it in a row
  const rowGroups = [...Array(Math.ceil(Object.keys(products).length / 4))];
  const productRows = rowGroups.map((row, i) => (
    Object.values(products).slice(i * 4, (i * 4) + 4)
  ));

  productRows.forEach((row, i) => {
    productList.push(
      <Row justify="space-between" className="product-list__product-row" key={`row-${i}`}>
        {row.map((product) => (
          <Col xs={24} md={6} key={product.id}>
            <ProductCard product={product} />
          </Col>
        ))}
      </Row>,
    );
  });

  return productList;
};

function ProductList({ products }: Props) {
  return (
    <div>
      <h2>Our Products</h2>
      {renderProductList(products)}
    </div>
  );
}

ProductList.defaultProps = {
  products: [],
};

const mapStateToProps = (state: RootState) => ({
  products: state.products,
});

export default connect(mapStateToProps)(ProductList);
