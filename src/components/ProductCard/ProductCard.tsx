import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Button, Card, InputNumber } from 'antd';
import { StarOutlined, StarFilled, ShoppingCartOutlined } from '@ant-design/icons';

import { addToCart } from '../../store/actions/cartActions';
import { addToWishList, removeFromWishList } from '../../store/actions/wishListActions';
import { formatPrice, productInWishList } from '../../utils';

import type { CartOrder, Product, WishListItem } from '../../types';
import type { RootState } from '../../store/store';

import './ProductCard.less';

const { Meta } = Card;

type Props = {
  product: Product,
  wishListItems: WishListItem[],
  dispatchCardAdd: (arg: CartOrder) => void,
  dispatchWishListAdd: (arg: number) => void,
  dispatchWishListRemove: (arg: number) => void,
};

type BuyActionProps = {
  id: number,
  dispatch: (arg: CartOrder) => void,
};

const BuyAction = ({ id, dispatch }: BuyActionProps) => {
  const [amount, setAmount] = useState<number | string>();

  const doAddToCart = () => {
    console.log(amount);
    if (typeof amount === 'number') {
      dispatch({
        productId: id,
        quantity: amount,
      });
    }
  };

  return (
    <div className="product-card__buy-inputgroup">
      <InputNumber type="number" min={1} onChange={(value) => setAmount(value)} />
      <Button type="primary" onClick={() => doAddToCart()} icon={<ShoppingCartOutlined />} />
    </div>
  );
};

export function ProductCard({
  product,
  wishListItems,
  dispatchCardAdd,
  dispatchWishListAdd,
  dispatchWishListRemove,
}: Props) {
  const {
    id,
    name,
    price,
    image,
  } = product;

  const favoriteAction = () => {
    if (productInWishList(id, wishListItems)) {
      return (
        <StarFilled key="edit" onClick={() => dispatchWishListRemove(id)} />
      );
    }
    return (
      <StarOutlined key="edit" onClick={() => dispatchWishListAdd(id)} />
    );
  };

  return (
    <div className="product-card">
      <Card
        hoverable
        style={{ width: 240 }}
        cover={<img alt="example" src={image} />}
        actions={[
          favoriteAction(),
          <BuyAction id={id} dispatch={dispatchCardAdd} />,
        ]}
      >
        <Meta title={name} description={formatPrice(price)} />
      </Card>
    </div>
  );
}

const mapStateToProps = (state: RootState) => ({
  wishListItems: state.wishList.items,
});

const mapDispatchToProps = (dispatch: any) => ({
  dispatchCardAdd: (order: CartOrder) => dispatch(addToCart(order)),
  dispatchWishListAdd: (id: number) => dispatch(addToWishList(id)),
  dispatchWishListRemove: (id: number) => dispatch(removeFromWishList(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductCard);
